[![pipeline status](https://gitlab.com/fredypasaud/story-four/badges/master/pipeline.svg)](https://gitlab.com/fredypasaud/story-four/commits/master)

[![coverage report](https://gitlab.com/fredypasaud/story-four/badges/master/coverage.svg)](https://gitlab.com/fredypasaud/story-four/commits/master)

## Author
Fredy Pasaud Marolan Silitonga

## Link Heroku
https://fredyweb.herokuapp.com/
