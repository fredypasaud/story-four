from django.contrib import admin
from .models import Schedule, Feedback

admin.site.register(Schedule)
admin.site.register(Feedback)

# Register your models here.
