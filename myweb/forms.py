from django import forms
from .models import Schedule, Feedback
from django.forms import widgets


class ScheduleForms (forms.ModelForm):

    class Meta:
        model = Schedule
        fields = ['nama_kegiatan','tanggal','tempat','kategori']
        widgets = {
            'nama_kegiatan' : forms.Textarea(attrs={'cols': 20, 'rows': 5}),
            'tanggal': forms.DateInput(attrs={'type': 'date'}),

         }

    def __init__(self, *args, **kwargs):
        super(ScheduleForms,self).__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

class FeedbackForms (forms.ModelForm):

    class Meta:
        model = Feedback
        fields = ['email','feedback']
        widgets = {
        'feedback' : forms.Textarea(attrs={'cols' : 20, 'rows': 5}),
        }

    def __init__(self, *args, **kwargs):
        super(FeedbackForms,self).__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
