from django.db import models

# Create your models here.

class Schedule(models.Model):
	# hari = models.CharField(max_length=8)
	tanggal = models.DateField()
	jam = models.TimeField(auto_now_add=True)
	nama_kegiatan = models.CharField(max_length=30)
	tempat = models.CharField(max_length=30)
	kategori = models.CharField(max_length=20)

	def __str__(self):
		return self.nama_kegiatan

class Feedback(models.Model):
	email = models.EmailField(max_length=70,blank=True, null= True, unique= True)
	feedback = models.CharField(max_length = 300)
	date = models.DateTimeField(auto_now_add= True)

	def __str__(self):
		return self.email
