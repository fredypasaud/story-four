from django.test import TestCase, Client, LiveServerTestCase, RequestFactory
from django.urls import resolve
from django.contrib.auth.models import User, AnonymousUser
from datetime import datetime
from django.utils.timezone import now
import time
from django.contrib.auth import authenticate

from .models import Schedule, Feedback
from . import views
from .forms import FeedbackForms, ScheduleForms

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options


class UnitTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='jacob', email='jacob@…', password='top_secret')

    def test_details(self):
        request = self.factory.get('/login/')
        request.user = self.user
        response = views.user_login(request)
        userlog = authenticate(request.user)
        self.assertEqual(response.status_code, 200)

    def test_landing(self):
        response = self.client.get('/')
        self.assertContains(response, "Hello, I'm Fredy", html=True)
        self.assertTemplateUsed(response,'landing.html')

    def test_about(self):
        response = self.client.get('/about/')
        self.assertContains(response, "Greetings", html=True)
        self.assertTemplateUsed(response,'about.html')

    def test_porto(self):
        response = self.client.get('/portofolio/')
        self.assertContains(response, "My Skills", html=True)
        self.assertTemplateUsed(response,'portofolio.html')

    def test_sched_view(self):
        response = self.client.get('/schedule/')
        self.assertEqual(response.status_code, 200)


    def create_feedback(self):
        new_feedback = Feedback.objects.create(email = "cobacoba@email.com", feedback = "coba coba")
        return new_feedback

    def test_check_status(self):
        c = self.create_feedback()
        self.assertTrue(isinstance(c, Feedback))
        self.assertTrue(c.__str__(), c.email)
        counting_all_feedback = Feedback.objects.all().count()
        self.assertEqual(counting_all_feedback, 1)

    def create_schedule(self):
        model_data = {
        "tanggal" : datetime.now(),
        "nama_kegiatan" : "kegiatan coba",
        "tempat" : "coba coba tempat",
        "kategori" : "coba coba kategori",
        }
        new_schedule = Schedule.objects.create(tanggal = datetime.now(),nama_kegiatan = "kegiatan coba",tempat = "coba coba tempat",kategori = "coba coba kategori",)
        return new_schedule

    def test_check_sched(self):
        c = self.create_schedule()
        self.assertTrue(isinstance(c, Schedule))
        self.assertTrue(c.__str__(), c.nama_kegiatan)


    def test_correct_views(self):
        landing = resolve('/')
        about = resolve('/about/')
        contact = resolve('/contact/')
        portofolio = resolve('/portofolio/')
        self.assertEqual(landing.func, views.landing)
        self.assertEqual(about.func, views.about)
        self.assertEqual(contact.func, views.contact)
        self.assertEqual(portofolio.func, views.portofolio)

    def test_form(self):
        form_data = {
        'email' : 'onlytest@gmail.com',
        'feedback' : 'only testing',
        }
        form = FeedbackForms(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/contact/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/contact/')
        self.assertEqual(response.status_code, 200)

    def test_form_sched(self):
        form_data = {
        "tanggal" : "11/07/2019",
        "nama_kegiatan" : "kegiatan coba",
        "tempat" : "coba coba tempat",
        "kategori" : "coba coba kategori",
        }
        form = ScheduleForms(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/schedule/create/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/schedule/create/')
        self.assertEqual(response.status_code, 200)

    def test_sched_det(self):
        c = self.create_schedule()
        resp = self.client.get('/schedule/1/')
        self.assertEqual(resp.status_code, 200)


class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        # firefox
        super(FunctionalTest, self).setUp()
        firefox_options = Options()
        firefox_options.add_argument('--no-sandbox')
        firefox_options.add_argument('--headless')
        firefox_options.add_argument('--disable-gpu')
        self.browser = webdriver.Firefox(firefox_options = firefox_options)

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_login(self):
        self.browser.get('http://127.0.0.1:8000/login')
        username = self.browser.find_element_by_id("username")
        password = self.browser.find_element_by_id("password")
        submit = self.browser.find_element_by_name("submit")
        username.send_keys("guest_service")
        password.send_keys("guest12345")
        submit.click()
        logout = self.browser.find_element_by_id("logoutButt")
        logout.click()
