from django.urls import include,path
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('', views.landing, name = 'index'),
   	path('about/', views.about, name = 'about'),
   	path('contact/', views.contact, name ='contact'),
   	path('portofolio/', views.portofolio, name ='portofolio'),
    path('schedule/create/',views.schedule_create,name ='schedule_create'),
    path('schedule/',views.schedule,name = 'schedule_list'),
    path('schedule/<int:pk>/', views.schedule_detail, name='schedule_detail'),
    path('delete-all', views.schedule_delete,name = 'schedule_delete'),
    path('delete-sel/<id>', views.schedule_delete_selected, name = 'delete_selected'),
    path('login', views.user_login,name='login'),
    path('logout', views.user_logout,name='logout'),
]
