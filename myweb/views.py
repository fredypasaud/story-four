from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse
from .models import Schedule, Feedback
from .forms import ScheduleForms, FeedbackForms
from django.contrib.auth import login, authenticate,logout


response = {}


def landing(request):
    return render (request,'landing.html')

def about(request):
    context = {"about_page": "active"}
    return render (request, 'about.html',context)

def contact(request):
    if request.method == "POST":
        form_feed = FeedbackForms(request.POST)
        if form_feed.is_valid():
            feedback = form_feed.save(commit=False)
            feedback.save()
            return redirect('contact')
    else:
        form_feed = FeedbackForms()
    return render(request, 'contact.html', {'form_feed' : form_feed, "contact_page" : "active",})

def portofolio(request):
    context = {"portofolio_page" : "active"}
    return render (request, "portofolio.html",context)

def schedule_create(request):
    if request.method == "POST":
        form = ScheduleForms(request.POST)
        if form.is_valid():
            schedule = form.save(commit=False)
            schedule.save()
            return redirect('schedule_detail', pk=schedule.pk)
    form = ScheduleForms()
    return render(request, 'schedule.html', {'form': form, "schedule_page" : "active", })

def schedule_detail(request,pk):
    schedule = Schedule.objects.get(pk=pk)
    return render(request, 'schedule_details.html',{'schedule': schedule, "schedule_view" : "active",})

def schedule(request):
    schedules = Schedule.objects.all()
    return render(request,'schedule_list.html',{'schedules' : schedules, "schedule_view" : "active"})

def schedule_delete(request):
    Schedule.objects.all().delete()
    return redirect(schedule)

def schedule_delete_selected(request,id):
    deleted = Schedule.objects.get(id=id)
    deleted.delete()
    return render(request, 'delete_view.html')

def user_login(request):
    response['login_view'] = "active"
    if request.method == "POST":
         username = request.POST['username']
         password = request.POST['password']
         user = authenticate(request, username=username, password=password)
         if user is not None:
            login(request, user)
            request.session['username'] = username
            response['username'] = request.session['username']
            return redirect('login')
         else:
            return HttpResponse("Invalid Password or Username")
    else:
        return render(request, 'login.html',response)

def user_logout(request):
    request.session.flush()
    logout(request)
    return redirect('login')
